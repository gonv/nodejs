// https://nodejs.org/api/modules.html
var fs = require('fs');

console.log('Writing a file');

//r - read file
//w - write file
//a - append file 

options = {flag: 'wx+'} //JavaScript Object 
                        //Notation

fs.writeFile('out.txt', 'Xpto', options, (err) => {
    if(err) return console.error(err);
});


fs.readFile('a.txt', function(err, data){
    if(err) return console.error(err); 
    console.log(data.toString());
});