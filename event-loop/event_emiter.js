// snake_case : any thing
// kebab-case : project name
// lowerCamelCase : cannot be user for project name, tipical on variables e functions
// UpperCamelCase, PascalCase : cannot be user for project name, tipical on Classes


// We normally only do the callbacks - the listeners
var listener1 = function() {
    console.log('listener1 executed');
}

var listener2 = function() {
    console.log('listener2 executed');
}

// ---- Normally we don't do the next part - is inside the modules
var events = require('events');
var eventEmitter =  new events.EventEmitter();

eventEmitter.addListener('connection', listener1);
eventEmitter.addListener('connection', listener2);

var eventListeners = events.EventEmitter.listenerCount(eventEmitter, 'connection');
console.log(eventListeners + ' Listener(s) listening to connetion event');

console.log('before emit connection event');

eventEmitter.emit('connection'); // wakes the listener1 and listner2
console.log('after emit connection event');
// --------